NAME:=$(CI_PROJECT_NAME)
VERSION:=$(CI_COMMIT_REF_NAME)
BUILD_ARCH:=$(GO_BUILD_ARCH)
TIMENOW:=$(shell date +%s)
ifeq (${BUILD_ARCH},)
	# Looks like we are not running in the CI so default to current branch
	BUILD_ARCH:=linux
endif

ifeq (${VERSION},)
	# Looks like we are not running in the CI so default to current branch
	VERSION:=$(shell git rev-parse --abbrev-ref HEAD)
endif

ifeq ($(NAME),)
	# Looks like we are not running in the CI so default to current directory
	NAME:=$(notdir $(CURDIR))
endif

.PHONY: all docker-build docker-push docker-run run-dev

docker-build:  ## Build docker image with short git hash
	@ docker build -t blockzilla/${NAME}:${VERSION} .

docker-push: ## Upload image to repository
	@ docker push blockzilla/${NAME}:${VERSION}

docker-run: ## Upload image to repository
	@ docker run -it blockzilla/${NAME}:${VERSION}

dev: npm-install
	node index.js

npm-install:
	npm install