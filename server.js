var TinyURL = require('tinyurl');
const forge = require('node-forge');
const Tortoise = require('tortoise')
const redis = require('redis');
const db1 = require('./db')
const botQueue = process.env.MESSAGE_QUEUE || 'job-log';

// initialise redis
const redisClient = redis.createClient(process.env.REDIS_PORT || '6379', process.env.REDIS_HOST || 'localhost');
redisClient.on('connect', function () {
    console.log('Redis redisClient connected');
});
redisClient.on('error', function (err) {
    console.log('Something went wrong ' + err);
});

db1.createTables()

let today = new Date()
today.setDate(today.getDate() - 1)
today = today.toISOString().slice(0, 10)

console.log(today)

// initialise queues
const tortoise = new Tortoise(process.env.RABBIT_HOST || 'amqp://guest:guest@127.0.0.1:5672/')
tortoise.on(Tortoise.EVENTS.CONNECTIONCLOSED, () => {
    console.log('RabbitMQ connection closed')
})
tortoise.on(Tortoise.EVENTS.CONNECTIONDISCONNECTED, () => {
    console.log('RabbitMQ connection disconnected')
})
tortoise.on(Tortoise.EVENTS.CONNECTIONERROR, (err) => {
    console.log('RabbitMQ connection error', err)
})


// push message to queue
function pushToQueue(msg, queue) {
    console.log("pushing " + queue + " item to queue")
    tortoise
        .queue(queue, { durable: false })
        .publish(msg)
        .then(() => {
            console.log(queue + 'update received, published message to queue', {
                queue,
                msg
            })
        })
        .catch((err) => {
            console.log('error occourred publishing to queue', {
                queue,
                err
            })
            throw err
        })
    return;
}

// build a message for telegram queue
function buildTelegramBotMessage(data) {
    data = data[0];
    var msg = { 'message': '' };
    var tmp = data.title + ' \n';
    console.log("Building Telegram message")
    // shorten URLs
    TinyURL.shorten(data.url, function (res, err) {
        if (err) {
            console.log(err)
        }
        msg.message = tmp + res;
        pushToQueue(msg, botQueue);
    });
    return;
}

var casesObj = {
    "hash": "",
    "cases": []
}

// query the DB and update redis
function updateCaseCache() {
    try {
        db1.query('select town as town, province as province, country as country, date as date, lat as lat, lon as lon, confirmed as confirmed, dead as dead, recovered as recovered, active as active, key as id from cases where date = $1', [today], (err, res) => {
            if (err) {
                console.log(err.stack)
            } else {
                if (res.rows.length > 0) {
                    res.rows.forEach(row => {
                        casesObj.cases.push(row);
                    })
                    console.log(casesObj)
                    casesObj.hash = hashCode(casesObj.cases)
                    redisClient.set('cases', JSON.stringify(casesObj), redis.print);
                    pushToQueue({}, "cases");
                } else {
                    console.log("node results")
                }
            }
        })
    } catch (err) {
        console.log('Retrying', err.message);
    }
    return;
}

var summaryObj = {
    "hash": "",
    "twitter": {
        "sentiment": "Neutral"
    },
    "cases": {
        "confirmed": "",
        "deaths": "",
        "recovered": "",
        "countries": "",
        "active": ""
    },
    "topCountries": []
}

// query the DB and update redis
function updateStatsCache() {
    console.log("reading stats data");
    try {
        db1.query('select SUM(total_confirmed) as confirmed, SUM(total_dead) as dead, SUM(total_recovered) as recovered, SUM(total_active) as active, COUNT(countries) as countries from countries where date = $1', [today], (err, res) => {
            if (err) {
                console.log(err.stack)
            } else {
                if (res.rows.length > 0) {
                    res.rows.forEach(row => {
                        summaryObj.cases.recovered = numberWithCommas(row.recovered)
                        summaryObj.cases.deaths = numberWithCommas(row.dead)
                        summaryObj.cases.active = numberWithCommas(row.active)
                        summaryObj.cases.countries = numberWithCommas(row.countries)
                        summaryObj.cases.confirmed = numberWithCommas(row.confirmed)
                    })
                    db1.query('select * from countries where date = $1 order by total_confirmed DESC', [today], (err, res) => {
                        if (err) {
                            console.log(err.stack)
                        } else {

                            if (res.rows.length > 0) {
                                var sum = 0;
                                res.rows.forEach(row => {
                                    sum += row.total_confirmed
                                    summaryObj.topCountries.push(row);
                                })
                                // TODO: calc percentage, move to go code
                                for (i in summaryObj.topCountries) {
                                    val = summaryObj.topCountries[i].total_confirmed;
                                    percentage = (val / sum) * 100;
                                    summaryObj.topCountries[i].total_percentage = percentage.toFixed(2)
                                    summaryObj.topCountries[i].total_confirmed = numberWithCommas(summaryObj.topCountries[i].total_confirmed)
                                }
                                summaryObj.hash = hashCode(summaryObj)
                                redisClient.set('summary', JSON.stringify(summaryObj), redis.print);
                                pushToQueue({}, "summary");
                                setTimeout(function () {
                                    process.exit(0);
                                }, 5000)

                            } else {
                                console.log("node results")
                            }
                        }
                    });
                } else {
                    console.log("node results")
                }
            }
        })

    } catch (err) {
        console.log('Retrying', err.message);
    }
    return;
}

function numberWithCommas(x) {
    if (typeof x === 'undefined' || x === null) {
        return 0
    }
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

// create a hash of the the input string
function hashCode(s) {
    var md = forge.md.sha1.create();
    md.update(s);
    return md.digest().toHex();
}

// load data and update cache
updateCaseCache()
updateStatsCache()
setTimeout(function () {
    process.exit(0);
}, 60000);

console.log('version 0.0.1')