const { Pool } = require('pg')

const connectionString = process.env.DB_CONNECTION || 'postgresql://root:password@localhost:3211/corona'
const pool = new Pool({
    connectionString: connectionString,
})

module.exports = {
    query: (text, params, callback) => {
        const start = Date.now()
        return pool.query(text, params, (err, res) => {
            const duration = Date.now() - start
            if (res && res.rowCount != null) {
                console.log('executed query', { text, duration, rows: res.rowCount })
            } else {
                console.log('executed query', { text, duration })
            }

            callback(err, res)
        })
    },
    createTables: () => {
        // pool.query('CREATE TABLE IF NOT EXISTS news(id TEXT PRIMARY KEY, json TEXT NOT NULL, date TEXT NOT NULL)', (err, res) => {
        //     if (err) {
        //         console.log(err)
        //         return next(err)
        //     }
        //     console.log("'news' table created")
        // });
        // pool.query('CREATE TABLE IF NOT EXISTS cases (id SERIAL PRIMARY KEY, location text, country text NOT NULL, date date NOT NULL, lat integer, lon integer, confirmed integer NOT NULL, dead integer NOT NULL, recovered integer NOT NULL, active integer NOT NULL, sync_date date)', (err, res) => {
        //     if (err) {
        //         console.log(err)
        //         return next(err)
        //     }
        //     console.log("'cases' table created")
        // });
    },
    getClient: (callback) => {
        pool.connect((err, client, done) => {
            const query = client.query
            // monkey patch the query method to keep track of the last query executed
            client.query = (...args) => {
                client.lastQuery = args
                return query.apply(client, args)
            }
            // set a timeout of 5 seconds, after which we will log this client's last query
            const timeout = setTimeout(() => {
                console.error('A client has been checked out for more than 5 seconds!')
                console.error(`The last executed query on this client was: ${client.lastQuery}`)
            }, 5000)
            const release = (err) => {
                // call the actual 'done' method, returning this client to the pool
                done(err)
                // clear our timeout
                clearTimeout(timeout)
                // set the query method back to its old un-monkey-patched version
                client.query = query
            }
            callback(err, client, release)
        })
    }
}